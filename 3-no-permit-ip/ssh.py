#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from netmiko import Netmiko


def pre_check(csv_data):
    device = {
        "host": csv_data[1],
        "username": csv_data[2],
        "password": csv_data[3],
        "device_type": csv_data[4],
    }
    rtr_1_commands = [
        "ip access-list extended allowed_network", "no permit ip any any"]
    rtr_2_commands = [
        "ip access-list extended ALLOW_INET", "no permit ip any any"]
    try:
        print(f"Connect to device : {csv_data[0]}, ip : {device['host']}")
        net_connect = Netmiko(**device)
        write = open("outputs/"+csv_data[0]+".txt", "w")
        try:
            if "1" in csv_data[5]:
                output = net_connect.send_config_set(rtr_1_commands)
                write.write(f"\nAccess List Command Executed\n")
                write.write(output)
                write.write(f"\nChecking ip access list\n")
                output = net_connect.send_command(
                    "show ip access-list allowed_network | in any any")
                write.write(output)
            elif "2" in csv_data[5]:
                output = net_connect.send_config_set(rtr_2_commands)
                write.write(f"\nAccess List Command Executed\n")
                write.write(output)
                write.write(f"\nChecking ip access list\n")
                output = net_connect.send_command(
                    "show ip access-list ALLOW_INET | in any any")
                write.write(output)
        except:
            write.write(f"\nAccess List Command Failed Executed \n")

        write.close()
        net_connect.disconnect()

        log_device = {
            "devicename": csv_data[0],
            "ip": csv_data[1],
            "status": "success mamang"
        }
    except:
        log_device = {
            "devicename": csv_data[0],
            "ip": csv_data[1],
            "status": "error coy"
        }
    return log_device
