#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from netmiko import Netmiko


def pre_check(csv_data):
    device = {
        "host": csv_data[1],
        "username": csv_data[2],
        "password": csv_data[3],
        "device_type": csv_data[4],
    }
    commands = ["show interface vlan 1", "show dmvpn", "show ip route | i 172.17", "ping 172.17.10.9 source vlan 1", "show run | I nat",
                "ping 172.17.247.2 source vlan 1", "ping 172.17.254.18 source vlan 1", "ping 172.17.1.1 source vlan 1",
                "ping 172.17.254.10 source vlan 1", "ping 172.17.254.26 source vlan 1", "ping 172.17.254.30 source vlan 1",
                "traceroute 172.17.10.9", "telnet 172.17.10.9 80"]
    try:
        print(f"Connect to device : {csv_data[0]}, ip : {device['host']}")
        net_connect = Netmiko(**device)
        write = open("outputs/"+csv_data[0]+".txt", "w")

        for i in commands:
            try:
                output = net_connect.send_command(i)
                write.write(f"\n{i}\n")
                write.write(output)
            except:
                write.write(f"\nCommand : {i}, failed to sent\n")

        write.close()
        net_connect.disconnect()

        log_device = {
            "devicename": csv_data[0],
            "ip": csv_data[1],
            "status": "success mamang"
        }
    except:
        log_device = {
            "devicename": csv_data[0],
            "ip": csv_data[1],
            "status": "error coy"
        }
    return log_device
