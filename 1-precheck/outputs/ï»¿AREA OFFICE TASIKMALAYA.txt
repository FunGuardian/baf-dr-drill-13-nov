
show interface vlan 1
Vlan1 is up, line protocol is up 
  Hardware is EtherSVI, address is 5057.a858.1a80 (bia 5057.a858.1a80)
  Description: = LAN =
  Internet address is 10.40.2.1/24
  MTU 1500 bytes, BW 100000 Kbit/sec, DLY 100 usec, 
     reliability 255/255, txload 4/255, rxload 1/255
  Encapsulation ARPA, loopback not set
  Keepalive not supported 
  ARP type: ARPA, ARP Timeout 04:00:00
  Last input 00:00:00, output never, output hang never
  Last clearing of "show interface" counters 6d23h
  Input queue: 0/75/0/1 (size/max/drops/flushes); Total output drops: 0
  Queueing strategy: fifo
  Output queue: 0/40 (size/max)
  5 minute input rate 542000 bits/sec, 241 packets/sec
  5 minute output rate 1871000 bits/sec, 245 packets/sec
     21703905 packets input, 1548698373 bytes, 0 no buffer
     Received 231121 broadcasts (0 IP multicasts)
     0 runts, 0 giants, 0 throttles 
     0 input errors, 0 CRC, 0 frame, 0 overrun, 0 ignored
     23594890 packets output, 4164493278 bytes, 0 underruns
     0 output errors, 0 interface resets
     332268 unknown protocol drops
     0 output buffer failures, 0 output buffers swapped out
show dmvpn
Legend: Attrb --> S - Static, D - Dynamic, I - Incomplete
	N - NATed, L - Local, X - No Socket
	T1 - Route Installed, T2 - Nexthop-override
	C - CTS Capable, I2 - Temporary
	# Ent --> Number of NHRP entries with same NBMA peer
	NHS Status: E --> Expecting Replies, R --> Responding, W --> Waiting
	UpDn Time --> Up or Down Time for a Tunnel
==========================================================================

Interface: Tunnel10, IPv4 NHRP Details 
Type:Spoke, NHRP Peers:3, 

 # Ent  Peer NBMA Addr Peer Tunnel Add State  UpDn Tm Attrb
 ----- --------------- --------------- ----- -------- -----
     1 36.91.66.18         10.10.100.1    UP 01:10:02     S
     1 202.152.39.243      10.10.100.2    UP 01:10:02     S
     1 139.255.93.42       10.10.100.3    UP 01:09:39     S

Interface: Tunnel20, IPv4 NHRP Details 
Type:Spoke, NHRP Peers:1, 

 # Ent  Peer NBMA Addr Peer Tunnel Add State  UpDn Tm Attrb
 ----- --------------- --------------- ----- -------- -----
     1 202.152.63.10       10.10.104.1    UP 01:10:01     S

show ip route | i 172.17
      172.17.0.0/16 is variably subnetted, 9 subnets, 3 masks
D EX     172.17.0.0/16 [170/1536512] via 10.10.100.3, 01:09:56, Tunnel10
D EX     172.17.0.0/24 [170/1538816] via 10.10.104.1, 01:09:52, Tunnel20
D EX     172.17.1.0/24 [170/1538816] via 10.10.104.1, 01:09:52, Tunnel20
D EX     172.17.2.0/24 [170/1538816] via 10.10.104.1, 01:09:52, Tunnel20
D EX     172.17.3.0/24 [170/1538816] via 10.10.104.1, 01:09:52, Tunnel20
D EX     172.17.4.0/24 [170/1538816] via 10.10.104.1, 01:09:52, Tunnel20
D EX     172.17.5.0/24 [170/1538816] via 10.10.104.1, 01:09:52, Tunnel20
D EX     172.17.10.0/24 [170/1538816] via 10.10.104.1, 01:09:52, Tunnel20
D        172.17.254.16/29 [90/1538816] via 10.10.104.1, 01:09:52, Tunnel20
ping 172.17.10.9 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.10.9, timeout is 2 seconds:
Packet sent with a source address of 10.40.2.1 
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 24/25/28 ms
show run | I nat
 ip nat inside
 ip nat outside
 ip nat outside
ip nat inside source list 100 interface Vlan10 overload
ip nat inside source list GSM interface Vlan20 overload
ping 172.17.247.2 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.247.2, timeout is 2 seconds:
Packet sent with a source address of 10.40.2.1 
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 24/28/32 ms
ping 172.17.254.18 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.254.18, timeout is 2 seconds:
Packet sent with a source address of 10.40.2.1 
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 24/30/44 ms
ping 172.17.1.1 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.1.1, timeout is 2 seconds:
Packet sent with a source address of 10.40.2.1 
.....
Success rate is 0 percent (0/5)
ping 172.17.254.10 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.254.10, timeout is 2 seconds:
Packet sent with a source address of 10.40.2.1 
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 28/28/32 ms
ping 172.17.254.26 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.254.26, timeout is 2 seconds:
Packet sent with a source address of 10.40.2.1 
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 24/26/28 ms
ping 172.17.254.30 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.254.30, timeout is 2 seconds:
Packet sent with a source address of 10.40.2.1 
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 24/39/52 ms
traceroute 172.17.10.9
Type escape sequence to abort.
Tracing the route to 172.17.10.9
VRF info: (vrf in name/id, vrf out name/id)
  1 10.10.104.1 28 msec 28 msec 28 msec
  2 172.17.254.17 24 msec 24 msec 24 msec
  3 172.17.10.9 24 msec 24 msec 24 msec
  4  * 
    172.17.10.150 !H  * 
telnet 172.17.10.9 80
Trying 172.17.10.9, 80 ... 
% Destination unreachable; gateway or host down
