
show interface vlan 1
Vlan1 is up, line protocol is up 
  Hardware is EtherSVI, address is 442b.03f1.d220 (bia 442b.03f1.d220)
  Internet address is 1.2.3.1/30
  MTU 1500 bytes, BW 100000 Kbit/sec, DLY 100 usec, 
     reliability 255/255, txload 1/255, rxload 1/255
  Encapsulation ARPA, loopback not set
  Keepalive not supported 
  ARP type: ARPA, ARP Timeout 04:00:00
  Last input 00:00:00, output never, output hang never
  Last clearing of "show interface" counters 00:31:21
  Input queue: 0/75/0/0 (size/max/drops/flushes); Total output drops: 0
  Queueing strategy: fifo
  Output queue: 0/40 (size/max)
  5 minute input rate 0 bits/sec, 0 packets/sec
  5 minute output rate 0 bits/sec, 0 packets/sec
     1580 packets input, 123228 bytes, 0 no buffer
     Received 0 broadcasts (0 IP multicasts)
     0 runts, 0 giants, 0 throttles 
     0 input errors, 0 CRC, 0 frame, 0 overrun, 0 ignored
     1570 packets output, 122458 bytes, 0 underruns
     0 output errors, 0 interface resets
     0 unknown protocol drops
     0 output buffer failures, 0 output buffers swapped out
show dmvpn
Legend: Attrb --> S - Static, D - Dynamic, I - Incomplete
	N - NATed, L - Local, X - No Socket
	T1 - Route Installed, T2 - Nexthop-override
	C - CTS Capable, I2 - Temporary
	# Ent --> Number of NHRP entries with same NBMA peer
	NHS Status: E --> Expecting Replies, R --> Responding, W --> Waiting
	UpDn Time --> Up or Down Time for a Tunnel
==========================================================================

Interface: Tunnel10, IPv4 NHRP Details 
Type:Spoke, NHRP Peers:3, 

 # Ent  Peer NBMA Addr Peer Tunnel Add State  UpDn Tm Attrb
 ----- --------------- --------------- ----- -------- -----
     1 36.91.66.18         10.10.100.1    UP 05:13:56     S
     1 202.152.39.243      10.10.100.2    UP 05:14:00     S
     1 139.255.93.42       10.10.100.3    UP 05:13:56     S

Interface: Tunnel20, IPv4 NHRP Details 
Type:Spoke, NHRP Peers:1, 

 # Ent  Peer NBMA Addr Peer Tunnel Add State  UpDn Tm Attrb
 ----- --------------- --------------- ----- -------- -----
     1 202.152.63.10       10.10.104.1    UP 05:13:59     S

show ip route | i 172.17
172.17.0.0/16 is variably subnetted, 9 subnets, 3 masks
D EX     172.17.0.0/16 
D EX     172.17.0.0/24 [170/26880256] via 10.10.104.1, 05:13:59, Tunnel20
D EX     172.17.1.0/24 [170/26880256] via 10.10.104.1, 05:13:59, Tunnel20
D EX     172.17.2.0/24 [170/26880256] via 10.10.104.1, 05:13:59, Tunnel20
D EX     172.17.3.0/24 [170/26880256] via 10.10.104.1, 05:13:59, Tunnel20
D EX     172.17.4.0/24 [170/26880256] via 10.10.104.1, 05:13:59, Tunnel20
D EX     172.17.5.0/24 [170/26880256] via 10.10.104.1, 05:13:59, Tunnel20
D EX     172.17.10.0/24 [170/26880256] via 10.10.104.1, 05:13:59, Tunnel20
D        172.17.254.16/29 [90/26880256] via 10.10.104.1, 05:13:59, Tunnel20
ping 172.17.10.9 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.10.9, timeout is 2 seconds:
Packet sent with a source address of 1.2.3.1 
.....
Success rate is 0 percent (0/5)
show run | I nat
 ip nat inside
 ip nat inside
 ip nat outside
ip nat inside source list ALLOW_INET interface Port-channel1.12 overload
ping 172.17.247.2 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.247.2, timeout is 2 seconds:
Packet sent with a source address of 1.2.3.1 
.....
Success rate is 0 percent (0/5)
ping 172.17.254.18 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.254.18, timeout is 2 seconds:
Packet sent with a source address of 1.2.3.1 
.....
Success rate is 0 percent (0/5)
ping 172.17.1.1 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.1.1, timeout is 2 seconds:
Packet sent with a source address of 1.2.3.1 
.....
Success rate is 0 percent (0/5)
ping 172.17.254.10 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.254.10, timeout is 2 seconds:
Packet sent with a source address of 1.2.3.1 
.....
Success rate is 0 percent (0/5)
ping 172.17.254.26 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.254.26, timeout is 2 seconds:
Packet sent with a source address of 1.2.3.1 
.....
Success rate is 0 percent (0/5)
ping 172.17.254.30 source vlan 1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.17.254.30, timeout is 2 seconds:
Packet sent with a source address of 1.2.3.1 
.....
Success rate is 0 percent (0/5)
traceroute 172.17.10.9
Type escape sequence to abort.
Tracing the route to 172.17.10.9
VRF info: (vrf in name/id, vrf out name/id)
  1 10.10.104.1 16 msec 16 msec 16 msec
  2 172.17.254.17 16 msec *  16 msec
  3 172.17.10.9 16 msec 16 msec 16 msec
  4 172.17.10.150 !H  !H  * 
telnet 172.17.10.9 80
Trying 172.17.10.9, 80 ... 
% Destination unreachable; gateway or host down
