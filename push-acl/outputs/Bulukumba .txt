
Access List Command Executed
config term
Enter configuration commands, one per line.  End with CNTL/Z.
Bulukumba(config)#ip access-list extended RESTRICTED-ACCESS
Bulukumba(config-ext-nacl)#1 permit ip 192.168.140.0 0.0.0.255 any
Bulukumba(config-ext-nacl)#end
Bulukumba#
show ip access-lists RESTRICTED-ACCESS | include 1 permit
    1 permit ip 192.168.140.0 0.0.0.255 any