
Access List Command Executed
config term
Enter configuration commands, one per line.  End with CNTL/Z.
Kayu_Agung(config)#ip access-list extended RESTRICTED-ACCESS
Kayu_Agung(config-ext-nacl)#1 permit ip 192.168.140.0 0.0.0.255 any
Kayu_Agung(config-ext-nacl)#end
Kayu_Agung#
show ip access-lists RESTRICTED-ACCESS | include 1 permit
    1 permit ip 192.168.140.0 0.0.0.255 any