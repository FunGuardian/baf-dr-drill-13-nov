#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from netmiko import Netmiko


def pre_check(csv_data):
    device = {
        "host": csv_data[1],
        "username": csv_data[2],
        "password": csv_data[3],
        "device_type": csv_data[4],
    }
    commands = ["ip access-list extended RESTRICTED-ACCESS",
                "1 permit ip 192.168.140.0 0.0.0.255 any"]
    try:
        print(f"Connect to device : {csv_data[0]}, ip : {device['host']}")
        net_connect = Netmiko(**device)
        write = open("outputs/"+csv_data[0]+".txt", "w")
        try:
            output = net_connect.send_config_set(commands)
            write.write(f"\nAccess List Command Executed\n")
            write.write(output)
            output = net_connect.send_command(
                "show ip access-lists RESTRICTED-ACCESS | include 1 permit")
            write.write(
                "\nshow ip access-lists RESTRICTED-ACCESS | include 1 permit\n")
            write.write(output)
        except:
            write.write(f"\nAccess List Command Failed Executed \n")

        write.close()
        net_connect.disconnect()

        log_device = {
            "devicename": csv_data[0],
            "ip": csv_data[1],
            "status": "success mamang"
        }
    except:
        log_device = {
            "devicename": csv_data[0],
            "ip": csv_data[1],
            "status": "error coy"
        }
    return log_device
